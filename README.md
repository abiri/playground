## EBC

EBC Monitoring (entire deployment): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fabiri%2Fplayground.git/HEAD?filepath=ebc%2Febc_monitoring.ipynb)

EBC Monitoring (weekly): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fabiri%2Fplayground.git/HEAD?filepath=ebc%2Febc_monitoring_7d.ipynb)