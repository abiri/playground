# -*- coding: utf-8 -*
# Data processing for the EBC project
#
# Author: abiri
# Date:   17.09.20

import logging
import os
import configparser
import numpy as np
import pandas as pd
import datetime as dt
from random import randrange
from statistics import mean, median
from IPython.display import display

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# Default params for command line arguments that are mandatory
DEFAULT_CONFIG_FILE   = '../ebc.conf'
DEFAULT_LOG_LEVEL     = 'ERROR'

DATE_FORMAT  = "%d/%m/%Y"
TIME_FORMAT  = "%H:%M:%S"

S_TO_US = 1000000

MAX_NR_OF_NODES_PER_DETECTION  = 30

DEFAULT_MAX_EVT_PROPAGATION_MS = 100

# ----------------------------------------------------------------------------------------------------------------------
# Classes and functions
# ----------------------------------------------------------------------------------------------------------------------


class EBCDataProcessor:

    def __init__(self, data_filename=None):
        self._logger = logging.getLogger(self.__class__.__name__)

        self.filename = data_filename  # type: str

        config_file      = DEFAULT_CONFIG_FILE
        log_file_name    = None
        log_file_level   = None
        log_stream_level = DEFAULT_LOG_LEVEL
        log_format       = None
        log_date_format  = None

        # Check configuration file
        if not os.path.isfile(config_file):
            raise TypeError('Config file (%s) not found' % (config_file,))

        config_file = os.path.abspath(config_file)

        # Read config file for other options
        config = configparser.SafeConfigParser()
        config.optionxform = str  # Case sensitive
        config.read(config_file)

        section_common = 'ebc'
        try:
            # Read options from config
            for name, value in config.items(section_common):
                value = value.strip()
                if value != '':
                    if   name == 'log_file_name':
                        if not isinstance(value, str):
                            raise TypeError('Log file name must be of type str: %s' % (value,))
                        else:
                            log_file_name = value
                    elif name == 'log_file_level':
                        if not isinstance(value, str):
                            raise TypeError('Log file level must be of type str: %s' % (value,))
                        else:
                            log_file_level = value
                    elif name == 'log_stream_level':
                        if not isinstance(value, str):
                            raise TypeError('Log stream level must be of type str: %s' % (value,))
                        else:
                            log_stream_level = value
                    elif name == 'log_format':
                        if not isinstance(value, str):
                            raise TypeError('Log format must be of type str: %s' % (value,))
                        else:
                            log_format = value
                    elif name == 'log_date_format':
                        if not isinstance(value, str):
                            raise TypeError('Log date format must be of type str: %s' % (value,))
                        else:
                            log_date_format = value
                    else:
                        self._logger.warning('Unknown config option in section [%s]: %s' % (section_common, name,))
        except configparser.NoSectionError:
            raise TypeError('No [%s] section specified in %s' % (section_common, config_file,))

        # Initialize logging - afterwards, can use "self._logger.*" instead of root logger ("logging.*")
        self._init_logging(log_file_name, log_file_level, log_stream_level, log_format, log_date_format)

    def _init_logging(self, log_file_name=None, log_file_level=None, log_stream_level=None, format=None, date_format=None):
        self._logger.setLevel(logging.DEBUG)
        self._logger.propagate = False  # Avoid propagation to root logger

        # Setup file handler for logging
        if log_file_name is not None and log_file_level is not None:
            fh = logging.FileHandler(filename=log_file_name)
            fh.setLevel(log_file_level)
        else:
            fh = None

        # Setup console output
        if log_stream_level is not None:
            ch = logging.StreamHandler()
            ch.setLevel(log_stream_level)
        else:
            ch = None

        # Create formatter and add it to the handlers
        if format is not None:
            if date_format is None:
                formatter = logging.Formatter(format)
            else:
                formatter = logging.Formatter(fmt=format, datefmt=date_format)

            if fh is not None:
                fh.setFormatter(formatter)
            if ch is not None:
                ch.setFormatter(formatter)

        # Add handlers to logger
        if fh is not None:
            self._logger.addHandler(fh)
        if ch is not None:
            self._logger.addHandler(ch)

    def print_df_stats(self, df=None):

        str_base = "\nData Frame information:\n"

        if df is None:
            self._logger.debug(str_base + "  Empty")
        else:

            # Print dimensions
            str_base += "  Dimensions: %i x %i\n" % (df.shape[0], df.shape[1],)

            # Print columns
            str_base += "  Columns: %s\n" % (', '.join(map(str, df.columns)),)

            # Print data types
            # str_base += "  Data type:\n%s\n" % (dataframe.dtypes,)

            self._logger.debug(str_base)

    def compute_event_stats(self, df=None, propagation_ms=DEFAULT_MAX_EVT_PROPAGATION_MS, auto_extend_evt=False, dates_excluded=None):

        if df is None:
            self._logger.warning("Data frame for co-detection statistics analysis is empty")
        else:
            self._logger.info("Maximal propagation duration %ims %s auto-extension" % (propagation_ms, 'with' if auto_extend_evt else 'without'))

            if 'start_time' not in df.columns:
                self._logger.warning("Incorrect data frame, missing column %s; aborting analysis" % ('start_time',))
            elif 'end_time' not in df.columns:
                self._logger.warning("Incorrect data frame, missing column %s; aborting analysis" % ('end_time',))

            # Make sure dataframe is ordered according to start_time, and set start_time as index
            data_sorted = df.sort_values(by=['start_time'])
            data_sorted.set_index('start_time', drop=False, inplace=True)

            # Sanitize data
            data_sorted = self.sanitize_data(data_sorted, dates_excluded)

            # Prepare loop state
            curr_codet      = []
            curr_nodes      = []
            curr_start_time = data_sorted.iloc[0].at['start_time']     # Start of the current co-detection
            curr_end_time   = data_sorted.iloc[0].at['end_time']       # Time at which all events of the same co-detection have finished
            curr_stop_time  = curr_start_time + propagation_ms * 1000  # Time at which we do not consider a new event part of the same co-detection

            # Find co-detections
            rows_list = []
            for i in data_sorted.index:

                # Check if new co-detection
                if data_sorted.at[i, 'start_time'] > curr_stop_time:

                    # Update delta to include entire co-detection
                    curr_delta = [curr_start_time,
                                  curr_start_time + (curr_codet[len(curr_codet) - 1] if (len(curr_codet) > 1) else 0),
                                  curr_end_time]

                    # Fetch new times
                    curr_start_time = data_sorted.at[i, 'start_time']
                    curr_end_time   = data_sorted.at[i, 'end_time']
                    curr_stop_time  = data_sorted.at[i, 'start_time'] + propagation_ms * 1000

                    if auto_extend_evt:
                        curr_stop_time = max(curr_stop_time, data_sorted.at[i, 'end_time'])

                    # Add statistics
                    if len(curr_codet) > 1:
                        codet_offsets = curr_codet[1:]
                        row = {'nr_nodes':      len(curr_codet),
                               'timestamp':     curr_delta[0],
                               'offset_min':    min(codet_offsets),
                               'offset_avg':    mean(codet_offsets),
                               'offset_median': median(codet_offsets),
                               'offset_max':    max(codet_offsets),
                               'delta_start':   curr_start_time - curr_delta[0],
                               'delta_last':    curr_start_time - curr_delta[1],
                               'delta_end':     curr_start_time - curr_delta[2],
                               'evt_duration':  curr_delta[2]   - curr_delta[0]}

                        rows_list.append(row)

                    # Start of new detection
                    curr_codet = [data_sorted.at[i, 'start_time']]  # Index 0: Unix timestamp in us
                    curr_nodes = [data_sorted.at[i, 'device_id']]
                else:
                    # Append to previous detection
                    node_id = data_sorted.at[i, 'device_id']
                    if node_id not in curr_nodes:
                        # Ignore multiple events from same node
                        curr_codet.append(data_sorted.at[i, 'start_time'] - curr_start_time)
                        curr_nodes.append(node_id)

                        # Update end time
                        curr_end_time = max(curr_end_time, data_sorted.at[i, 'end_time'])

                        # Adjust stop time if desired and necessary
                        if auto_extend_evt:
                            curr_stop_time = max(curr_stop_time, data_sorted.at[i, 'end_time'])

                # Show progress every 10%
                if not (data_sorted.index.get_loc(i) % int(data_sorted.shape[0] / 10)):
                    self._logger.debug("Status of event analysis: {0:3d}%".format(10 * int(data_sorted.index.get_loc(i) / int(data_sorted.shape[0] / 10)),))

            # Create dataframe
            stats = pd.DataFrame(rows_list)

            return stats

    def print_event_stats(self, df=None, html_output=False):

        # Print offset statistics
        percentile_low  = 90
        percentile_high = 99
        df_columns      = ['Event statistic', 'Min [us]', 'Average [us]', 'Median [us]', '{:2d}% [us]'.format(percentile_low), '{:2d}% [us]'.format(percentile_high), 'Max [us]']

        rows_list = []
        str_base  = "\nEvent information:"

        if df is None:
            if html_output:
                print("Invalid dataframe for statistical analysis of events")
            else:
                self._logger.info(str_base + "  Empty")
        else:

            if html_output:
                print("Captured co-detections: {:d}".format(df.shape[0]))
            else:
                str_base += "\n  Captured co-detections: %d" % (df.shape[0],)
                str_base += "\nEvent statistics: \t     {1:s} | {2:s} |  {3:s} |     {4:s} |     {5:s} |     {6:s} |".format(*df_columns)

            for column in df.loc[:, 'offset_min':]:
                data = df[column]
                row = {'name':      column,
                       'min':       int(min(data)),
                       'avg':       int(mean(data)),
                       'median':    int(median(data)),
                       'perc_low':  int(np.percentile(data, percentile_low)),
                       'perc_high': int(np.percentile(data, percentile_high)),
                       'max':       int(max(data))}

                str_base += "\n  {0:13s}    \t {1:12d} | {2:12d} | {3:12d} | {4:12d} | {5:12d} | {6:12d} |".format(*[value for value in row.values()])
                rows_list.append(row)

            if html_output:
                event_stats         = pd.DataFrame(rows_list)
                event_stats.columns = df_columns
                display(event_stats)
            else:
                self._logger.info(str_base)

    def sanitize_data(self, df, dates_excluded=None, max_evt_duration_s=600):

        # Do not mutate original list
        data = df

        # Drop duplicates (incorrectly entered in GSN)
        data.drop_duplicates('start_time', inplace=True)
        data.sort_index(inplace=True)

        # Filter faulty start_times [us] and excessive trigger durations [s]
        min_date = dt.datetime(year=2010, month=1, day=1).timestamp() * S_TO_US
        corrupted_values = data.loc[:min_date].index.tolist() + data[data['trg_duration'] > max_evt_duration_s].index.tolist()
        data.drop(corrupted_values, inplace=True)

        # Filter dates that are not of interest / contain faulty data
        excluded_values = []
        if dates_excluded is not None:
            str_base = "\nExcluding dates:\n"

            for date in dates_excluded:
                if len(date) == 0 or len(date) > 2:
                    self._logger.warning("Expected start and optional end date, but received: %s" % (str(date),))
                else:
                    start_date = dt.datetime.strptime(date[0], DATE_FORMAT)
                    if len(date) > 1:
                        end_date = dt.datetime.strptime(date[1], DATE_FORMAT) + dt.timedelta(hours=23, minutes=59, seconds=59)
                    else:
                        end_date = start_date + dt.timedelta(hours=23, minutes=59, seconds=59)

                    if start_date > end_date:
                        self._logger.warning("Invalid time span to filter: %s - %s" % (start_date.strftime(DATE_FORMAT + " " + TIME_FORMAT), end_date.strftime(DATE_FORMAT + " " + TIME_FORMAT),))
                    else:
                        curr_excluded = data.loc[(start_date.timestamp()*S_TO_US):(end_date.timestamp()*S_TO_US)].index.tolist()
                        excluded_values.extend(curr_excluded)
                        str_base += "  %s - %s\n" % (start_date.strftime(DATE_FORMAT + " " + TIME_FORMAT), end_date.strftime(DATE_FORMAT + " " + TIME_FORMAT),)

            self._logger.debug(str_base)
            data.drop(excluded_values, inplace=True)

        return data

    def print_codet_stats(self, codet_list=None, html_output=False, force_print=False):

        nr_codetections = sum(len(codets) for codets in codet_list)
        df_columns      = ['Number of nodes', 'Number of co-detections', 'Percentage [%]']

        rows_list = []
        str_base  = "\nCo-detection statistics:\n"

        if codet_list is None:
            if html_output:
                print('No co-detections found')
            else:
                self._logger.debug(str_base + "  Empty")
                return 0
        else:

            for i in range(1, len(codet_list)):
                if len(codet_list[i]):
                    row = {'nr_nodes':   i,
                           'nr_codets':  len(codet_list[i]),
                           'percentage': (len(codet_list[i]) / nr_codetections * 100)}

                    str_base += "  Co-detections with {0:2d} nodes: {1:6d} ({2:5.2f}%)\n".format(row['nr_nodes'], row['nr_codets'], row['percentage'])
                    rows_list.append(row)

            if html_output:
                codet_stats         = pd.DataFrame(rows_list)
                codet_stats.columns = df_columns
                display(codet_stats)
            else:
                if force_print:
                    self._logger.info(str_base)
                else:
                    self._logger.debug(str_base)
                return sum((codet_list.index(codets) * len(codets)) for codets in codet_list)

    def find_codetections(self, df=None, propagation_ms=DEFAULT_MAX_EVT_PROPAGATION_MS, auto_extend_evt=False, dates_excluded=None):

        self._logger.debug("Analyzing data set for co-detections of maximal propagation duration %ims %s auto-extension" % (propagation_ms, 'with' if auto_extend_evt else 'without'))

        if 'start_time' not in df.columns:
            self._logger.warning("Incorrect data frame, missing column %s; aborting analysis" % ('start_time',))
        elif 'end_time' not in df.columns:
            self._logger.warning("Incorrect data frame, missing column %s; aborting analysis" % ('end_time',))

        # Make sure dataframe is ordered according to start_time, and set start_time as index
        data_sorted = df.sort_values(by=['start_time'])
        data_sorted.set_index('start_time', drop=False, inplace=True)

        # Sanitize data
        data_sorted = self.sanitize_data(data_sorted, dates_excluded)

        # Prepare loop state
        codetections     = []  # List of lists of lists: index contains list of co-detections with index
        curr_start_time  = data_sorted.iloc[0].at['start_time']
        curr_stop_time   = curr_start_time + propagation_ms * 1000
        curr_codetection = []
        curr_nodes       = []

        # Fill co-detections with empty lists to allow indexing
        for i in range(0, MAX_NR_OF_NODES_PER_DETECTION):
            codetections.append([])

        # Find co-detections
        for i in data_sorted.index:

            # Check if new co-detection
            if data_sorted.at[i, 'start_time'] > curr_stop_time:
                # Add old co-detection to list
                key = len(curr_codetection)
                codetections[key].append(curr_codetection)

                # Start of new detection
                curr_codetection = [data_sorted.at[i, 'start_time']]  # Index 0: Unix timestamp in us
                curr_nodes       = [data_sorted.at[i, 'device_id']]
                curr_start_time  = data_sorted.at[i, 'start_time']
                curr_stop_time   = data_sorted.at[i, 'start_time'] + propagation_ms * 1000

                if auto_extend_evt:
                    curr_stop_time = max(curr_stop_time, data_sorted.at[i, 'end_time'])
            else:
                # Append to previous detection
                node_position = data_sorted.at[i, 'device_id']
                if node_position not in curr_nodes:
                    # Ignore multiple events from same node
                    curr_codetection.append(data_sorted.at[i, 'start_time'] - curr_start_time)
                    curr_nodes.append(node_position)

                    # Adjust stop time if desired and necessary
                    if auto_extend_evt:
                        curr_stop_time = max(curr_stop_time, data_sorted.at[i, 'end_time'])

            # Show progress every 10%
            if not (data_sorted.index.get_loc(i) % int(data_sorted.shape[0]/10)):
                self._logger.debug("Status of co-detection analysis: {0:3d}%".format(10 * int(data_sorted.index.get_loc(i) / int(data_sorted.shape[0]/10)),))

        # Add last co-detection as well
        key = len(curr_codetection)
        codetections[key].append(curr_codetection)

        self._logger.debug("Finished co-detection analysis")

        # Print statistics
        counter = self.print_codet_stats(codetections)

        # If we dont reject samples because they are from a node that is already part of the co-detection, this should add up to the same
        self._logger.debug("Control number: Included %i data points out of %i" % (counter, data_sorted.shape[0]))
        return codetections

    def compare_metrics(self, test_input, test_output):

        if len(test_output) >= 1:
            results_tracing = test_output[0]
        else:
            self._logger.warning("No actuation data for analysis, aborting evaluation")
            return False
        if len(test_output) >= 2:
            results_serial = test_output[1]
        else:
            self._logger.warning("No serial data for analysis, aborting evaluation")
            return False
        if len(test_output) >= 3:
            results_power = test_output[2]
        else:
            self._logger.warning("No power data for analysis, ignoring metrics")
            results_power = None

        # Set 'test_id' as index to allow quick check whether all tests are available
        test_input.set_index('test_id', drop=False, inplace=True)
        test_input.sort_index(inplace=True)
        results_tracing.set_index('test_id', drop=False, inplace=True)
        results_serial.set_index('test_id', drop=False, inplace=True)
        if results_power is not None:
            results_power.set_index('test_id', drop=False, inplace=True)

        # Verify we have in- and output for each test
        if not test_input['test_id'].equals(results_tracing['test_id'].unique()) and test_input['test_id'].equals(results_serial['test_id'].unique()):
            self._logger.warning('Missing test IDs for comparison')

        # Compare each test input with its test output
        rows_list = []
        columns = ['Test ID', 'Event', 'Networks [#]', 'With leader [%]', 'Reporter [#]', 'ETE [%]', 'Healthy [%]',
                   'Sync Event [us]', 'Sync Wakeup [us]', 'Sync LE [us]', 'Sync Aggr [us]', 'Sync Dist [us]',
                   'Latency LE [us]', 'Latency Aggr [us]', 'Latency Rep [us]', 'Latency Dist [us]', 'Event Acc [us]',
                   'BS [uA]', 'Leader [uA]', 'Source [uA]']
        header_str = ' | '.join(columns) + ' |'
        output_str = ' | '.join(['{%d:%dd}' % (columns.index(i), len(i),) for i in columns]) + ' |'

        str_base = 'Metrics by event\n' + header_str + '\n'

        for test_in in test_input.itertuples():

            if (test_in.test_id not in results_tracing.index) or (test_in.test_id not in results_serial.index):
                self._logger.debug('Skipping test %d due to missing output' % (test_in.test_id,))
            else:

                # Prepare input
                offsets         = [codetection[1:]  for codetection in test_in.codetections]
                in_network_size = [len(codetection) for codetection in test_in.codetections]

                for evt in range(0, len(offsets)):
                    # Prepare output
                    curr_traces = results_tracing[(results_tracing['test_id'] == test_in.test_id) & (results_tracing['evt'] == evt)]
                    curr_serial = results_serial[ (results_serial['test_id']  == test_in.test_id) & (results_serial['evt']  == evt)]

                    # In case of serious errors where no node had sufficient traces, reporting is skipped
                    if (curr_traces.shape[0] == 0) or (curr_serial.shape[0] == 0):
                        self._logger.warning("Skipping event %d of test %d due to missing output, as only %d traces and %d lines of serial output" % (evt, test_in.test_id, curr_traces.shape[0], curr_serial.shape[0],))
                        continue

                    curr_sensor_ids = curr_serial['sensor_ids'].item()
                    curr_bs         = curr_serial['bs'].item()
                    curr_networks   = curr_serial['networks'].item()
                    curr_leaders    = curr_serial['leaders'].item()
                    curr_reporters  = curr_serial['actual_reporters'].item()

                    # Network metrics
                    true_leader  = max(curr_leaders) if len(curr_leaders) else 0
                    nr_networks  = len(curr_networks)
                    true_network = [network for network in curr_networks if network[0] == true_leader][0] if nr_networks else []
                    source_nodes = [i for i in curr_sensor_ids if (i != true_leader)]
                    with_leader  = int(sum([len(network[1:]) if (network[0] == true_leader) else 0 for network in curr_networks]) * 100/len(source_nodes)) if nr_networks else 0
                    nr_reporters = len(curr_reporters)
                    end_to_end   = int(curr_serial['ack'] * 100/len(curr_sensor_ids))
                    health_ack   = int(curr_serial['health_ack'] if pd.notna(curr_serial['health_ack'].item()) else 100)

                    # Timing
                    curr_start   = curr_traces.loc[curr_traces['start'] == 1, 'stag_wkup'].min()
                    sync_evt     = self.delta(curr_traces.loc[(curr_traces['start'] == 1) & (curr_traces['node_id'].isin(true_network)), 'stag_wkup'],   S_TO_US)
                    sync_wakeup  = self.delta(curr_traces.loc[(curr_traces['start'] == 0) & (curr_traces['node_id'].isin(true_network)), 'stag_wkup'],   S_TO_US)
                    sync_LE      = self.delta(curr_traces.loc[(curr_traces['start'] == 1) & (curr_traces['node_id'].isin(true_network)), 'leader_elec'], S_TO_US)
                    sync_aggr    = self.delta(curr_traces.loc[(curr_traces['start'] == 0) & (curr_traces['node_id'].isin(true_network)), 'data_aggr'],   S_TO_US)
                    sync_dist    = self.delta(curr_traces.loc[(curr_traces['start'] == 1) & (curr_traces['node_id'].isin(true_network)), 'dist'],        S_TO_US)
                    latency_LE   = self.delta(curr_traces.loc[ curr_traces['start'] == 0, 'leader_elec'], S_TO_US, curr_start)
                    latency_aggr = self.delta(curr_traces.loc[ curr_traces['start'] == 0, 'data_aggr'],   S_TO_US, curr_start)
                    latency_rep  = self.delta(curr_traces.loc[(curr_traces['start'] == 0) & (curr_traces['node_id'].isin(curr_reporters)), 'report'],    S_TO_US, curr_start)
                    latency_dist = self.delta(curr_traces.loc[ curr_traces['start'] == 0, 'dist'],        S_TO_US, curr_start)
                    evt_accuracy = self.delta(curr_traces.loc[ curr_traces['start'] == 1, 'evt_ts'], b=curr_traces.loc[curr_traces['start'] == 0, 'evt_ts'])

                    # Power
                    if results_power is not None and test_in.test_id in results_power.index:
                        curr_power     = results_power[(results_power['test_id'] == test_in.test_id) & (results_power['evt'] == evt)]
                        current_bs     = int(curr_power.loc[(curr_power['node_id'] == curr_bs),       'current_avg'].item()) if (curr_bs     != 0) else np.nan
                        current_leader = int(curr_power.loc[(curr_power['node_id'] == true_leader),   'current_avg'].item()) if (true_leader != 0) else np.nan
                        current_source = int(curr_power.loc[curr_power['node_id'].isin(source_nodes), 'current_avg'].mean()) if len(source_nodes)  else np.nan
                    else:
                        current_bs     = np.nan
                        current_leader = np.nan
                        current_source = np.nan

                    row = {'test_id': test_in.test_id, 'evt': evt, 'nr_networks': nr_networks, 'with_leader': with_leader, 'nr_rep': nr_reporters, 'ete': end_to_end, 'health': health_ack,
                           'sync_evt': sync_evt, 'sync_wkup': sync_wakeup, 'sync_le': sync_LE, 'sync_aggr': sync_aggr, 'sync_dist': sync_dist,
                           'lat_le': latency_LE, 'lat_aggr': latency_aggr, 'lat_rep': latency_rep, 'lat_dist': latency_dist, 'evt_acc': evt_accuracy,
                           'current_bs': current_bs, 'current_leader': current_leader, 'current_source': current_source}

                    rows_list.append(row)
                    str_base += output_str.format(*[value if (value is not None and not pd.isnull(value)) else 0 for value in row.values()]) + '\n'

        self._logger.info(str_base)

        # Overall metrics
        results = pd.DataFrame(rows_list)
        results_avg = []
        results_med = []
        results_min = []
        results_max = []

        for column in results.columns[2:]:
            if sum(results[column].notna()) > 0:
                avg_val = results[column].mean()
                med_val = results[column].median()
                min_val = results[column].min()
                max_val = results[column].max()
            else:
                self._logger.debug("Skipping column %s for overall metrics, as empty" % (column,))
                avg_val = med_val = min_val = max_val = 0

            results_avg.append(int(avg_val) if pd.notna(avg_val) else 0)
            results_med.append(int(med_val) if pd.notna(med_val) else 0)
            results_min.append(int(min_val) if pd.notna(min_val) else 0)
            results_max.append(int(max_val) if pd.notna(max_val) else 0)

        str_base  = 'Overall summary (0: Mean, 1: Median, 2: Minimum, 3: Maximum)\n' + header_str      + '\n'
        str_base += output_str.format(0, 0, *results_avg) + '\n'
        str_base += output_str.format(0, 1, *results_med) + '\n'
        str_base += output_str.format(0, 2, *results_min) + '\n'
        str_base += output_str.format(0, 3, *results_max) + '\n'

        self._logger.info(str_base)

    def extract_bs_data(self, health_df, start_date=None):

        rows_list = []

        # Extract start and end
        df = health_df.set_index('generation_time', drop=False)

        # Remove sniffer entries where retransmissions were detected; through different 'target_id's, same packet at different base stations will be kept
        df.drop_duplicates(inplace=True, subset=['generation_time', 'device_id', 'target_id'])

        if start_date is None:
            start_date = df['generation_time'].min()
        end_date   = df['generation_time'].max()
        time_delta = end_date - start_date

        self._logger.debug("Analysing health data from %s - %s" % (start_date.strftime(DATE_FORMAT + " " + TIME_FORMAT), end_date.strftime(DATE_FORMAT + " " + TIME_FORMAT),))

        # Go through data and find numbers of unique reports per base station
        bs_ids = set(df['target_id'].unique()) - {0}
        for bs_id in bs_ids:
            curr_data = df[df['target_id'] == bs_id]
            curr_report = start_date

            # Set second and minute to full hour
            curr_report.replace(minute=0, second=0, microsecond=0)

            while curr_report < end_date:

                next_report = curr_report + dt.timedelta(hours=1)
                row = {'bs_id': bs_id, 'generation_time': curr_report, 'nr_reports': curr_data.loc[curr_report:next_report].shape[0], 'fallback': curr_data['fallback'].any()}
                rows_list.append(row)

                # Set to next hour
                curr_report = next_report

        return pd.DataFrame(rows_list)

    @staticmethod
    def extract_clock_drift(health_df, min_id, reference_id=None):

        node_offset_us   = 6 * S_TO_US
        health_period_us = 3600 * S_TO_US

        # Get device IDs
        node_ids = health_df['device_id'].unique()

        for node_id in node_ids:
            curr_offset_us = (node_id - min_id) * node_offset_us
            mask           = health_df['device_id'] == node_id

            health_df.loc[mask, 'clock_drift_local']  = health_df.loc[mask, 'generation_time_microsec'].mod(health_period_us) - curr_offset_us
            health_df.loc[mask, 'clock_drift_global'] = health_df.loc[mask, 'timestamp'].mod(health_period_us / 1000) * 1000 - curr_offset_us

        # Correct nodes with negative drift (as modulo rounds them to the previous health period)
        mask_negative = health_df['clock_drift_local'] > 90/100 * health_period_us
        health_df.loc[mask_negative, 'clock_drift_local'] = health_df.loc[mask_negative, 'clock_drift_local'] - health_period_us

        mask_negative = health_df['clock_drift_global'] > 90/100 * health_period_us
        health_df.loc[mask_negative, 'clock_drift_global'] = health_df.loc[mask_negative, 'clock_drift_global'] - health_period_us

        if reference_id is not None:
            return health_df[health_df['target_id'] == reference_id]
        else:
            return health_df

    @staticmethod
    def gather_codetection_sample(codet_list, max_nr_events=1, min_nr_of_nodes=1, max_nr_of_nodes=MAX_NR_OF_NODES_PER_DETECTION):

        codet_candidates = []
        codet_chosen     = []

        # Create list of candidates
        for i in range(min_nr_of_nodes, max_nr_of_nodes + 1):
            if len(codet_list[i]):
                for codet in codet_list[i]:
                    codet_candidates.append(codet)

        # Make sure that we cannot over-sample
        max_nr_events = min(max_nr_events, len(codet_candidates))

        # Sample candidate list
        for i in range(0, max_nr_events):
            codet_chosen.append(codet_candidates.pop(randrange(0, len(codet_candidates))))

        return codet_chosen

    @staticmethod
    def delta(a, multiplier=1, b=None):
        if a is None or (a.shape[0] == 0) or a.isnull().all():
            return np.nan

        if b is None:
            return int(multiplier * (a.max() - a.min()))
        elif isinstance(b, pd.Series) or isinstance(b, pd.DataFrame):
            if (b.shape[0] == 0) or a.isnull().all():
                return np.nan
            else:
                return int(multiplier * (a.subtract(b)).abs().max())
        else:
            return int(multiplier * (a.max() - b))
